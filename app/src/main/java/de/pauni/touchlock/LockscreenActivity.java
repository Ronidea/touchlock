package de.pauni.touchlock;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Created by roni on 23.05.17.
 */

public class LockscreenActivity extends Activity{

    float offset;
    static float scale;
    double dragThreshold = 0.8;

    boolean stayAwakeActive = false;
    int now     = 0;    // delay for hiding the views
    int sooner  = 1700;
    int soon    = 2500;
    int w;
    int h;
    int mUIFlag = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;


    CardView        cv_unlockButton;
    LinearLayout    ll_unlockButton;
    RelativeLayout  ll_brightness;
    RelativeLayout  rl_root;


    Runnable hideViews;
    Handler handler = new Handler();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // make the app fullscreen
        getWindow().getDecorView().setSystemUiVisibility(mUIFlag);
        // set the layout's height and width
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        // keep the device awake
        switchStayAwake(rl_root);
        setContentView(R.layout.lockscreen_activity);



        // Window and measurement stuff
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        w = displayMetrics.widthPixels;
        h = displayMetrics.heightPixels;
        scale =  displayMetrics.density;


        // initialize views and set listeners
        initializeViews();
        setListeners();
        hideControls(soon);
    }


    private void setListeners() {


        // lock button listener
        ll_unlockButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float new_pos;
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        stopHiding();
                        offset = event.getRawX();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        // touchX, - fingerOffset in Button, + Marging of rl_root.
                        new_pos = event.getRawX()-offset+dpToPx(15);

                        if (new_pos < dpToPx(15) || new_pos+cv_unlockButton.getLayoutParams().height > w-dpToPx(15))
                            break;


                        updateUnlockButtonPosition(new_pos);
                        updateUnlockButtonColor(event.getRawX());
                        break;

                    case MotionEvent.ACTION_UP:
                        if (event.getRawX() > (dragThreshold * w)) { // if unlockbt reached finish point
                            finish();
                            LockService.launchNotification(getApplicationContext());
                        } else {
                            hideControls(soon);
                            updateUnlockButtonPosition(dpToPx(15)); // reset to startposition
                        }
                        break;
                }
                return false;
            }
        });

        rl_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideControls(false);    // make all controls visible
                stopHiding();           // stop previous "hide-counter"
                hideControls(sooner);   // hide them again in 2 seconds
            }
        });
    }
    private void initializeViews() {
        // init stuff
        cv_unlockButton = (CardView)        findViewById(R.id.cv_unlockButton);
        ll_unlockButton = (LinearLayout)    findViewById(R.id.ll_unlockButton);
        ll_brightness   = (RelativeLayout)  findViewById(R.id.ll_brightness_control);
        rl_root         = (RelativeLayout)  findViewById(R.id.rl_root);

        hideViews = new Runnable() {
            @Override
            public void run() {
                hideControls(true);
            }
        };
    }



    private void updateUnlockButtonPosition(float x) {
        cv_unlockButton.setX(x);
    }
    private void updateUnlockButtonColor(float position) {
        if (position<=0.8*w) {
            setTint(cv_unlockButton, R.color.bt_pos_0);
            return;
        }

        if (0.8*w <= position) {
            setTint(cv_unlockButton, R.color.bt_pos_4);
        }

    }
    private void setTint(View view, int color) {
        view.setBackgroundTintList(
                ColorStateList.valueOf(getResources().getColor(color)));
    }
    private void hideControls(boolean hide) {
        if (hide) {
            ll_brightness.setVisibility (View.GONE);
            cv_unlockButton.setVisibility     (View.GONE);
        } else {
            ll_brightness.setVisibility (View.VISIBLE);
            cv_unlockButton.setVisibility     (View.VISIBLE);
        }

    }
    private void hideControls(int hideInSeconds) {
        handler.postDelayed(hideViews, hideInSeconds);

    }
    private void stopHiding() {
        handler.removeCallbacks(hideViews);
    }



    public void brightnessLow   (View v) {
        rl_root.setBackgroundColor(getResources().getColor(R.color.brightnessLow));
        stopHiding();
        hideControls(sooner);
    }
    public void brightnessMedium(View v) {
        rl_root.setBackgroundColor(getResources().getColor(R.color.brightnessMedium));
        stopHiding();
        hideControls(sooner);
    }
    public void brightnessHigh  (View v) {
        rl_root.setBackgroundColor(getResources().getColor(R.color.brightnessHigh));
        stopHiding();
        hideControls(sooner);
    }
    public void fullscreenButton(View v) {
        int flags = View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        getWindow().getDecorView().setSystemUiVisibility(flags);
        getWindow().setLayout(w, h);

        stopHiding();
        hideControls(soon);
    }
    public void switchStayAwake (View v) {
        if (!stayAwakeActive) {
            stayAwakeActive = true;
            toast("Stay awake: enabled");
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            stayAwakeActive = false;
            toast("Stay awake: disabled");
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }


    @Override
    public void onBackPressed() { }
    @Override
    protected void onPause() {
        super.onPause();
        // if recentApps-button is pressed, close dialog and launch new notification
        finish();
        LockService.launchNotification(getApplicationContext());
    }

    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    static float dpToPx(int dps) {
        return (dps * scale + 0.5f);
    }
}
