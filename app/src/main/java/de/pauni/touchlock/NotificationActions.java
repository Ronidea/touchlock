package de.pauni.touchlock;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by roni on 23.05.17.
 */

public class NotificationActions extends Service {


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        switch(intent.getStringExtra("action")) {

            case "lock":
                startActivity(new Intent(this, LockscreenActivity.class));
                break;
            case "close":
                stopService(new Intent(getApplicationContext(), LockService.class));
                break;
        }


        // close notification. New notification is built, when lock screen is gone
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancelAll();
        // close the statusbar
        sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        // destroy this notification-service
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
