package de.pauni.touchlock;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by roni on 23.05.17.
 */

public class LockService extends Service {


    static boolean close = false;
    static PendingIntent piLock;
    static PendingIntent piClose;



    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("LockService", "created");

        launchNotification(this);
    }






    static void launchNotification(Context context) {

        // lock intent
        Intent intentLock = new Intent(context, NotificationActions.class);
        intentLock.putExtra("action", "lock");
        intentLock.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        piLock = PendingIntent.getService(context, 222, intentLock, PendingIntent.FLAG_ONE_SHOT);

        // close intent
        Intent intentClose = new Intent(context, NotificationActions.class);
        intentClose.putExtra("action", "close");
        piClose = PendingIntent.getService(context, 333, intentClose, PendingIntent.FLAG_ONE_SHOT);


        // notification icon
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ic_launcher);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle("Touchlock")
                .setLargeIcon(bitmap)
                .setSmallIcon(R.drawable.ic_lock_black_24dp)
                .setColor(context.getResources().getColor(R.color.colorPrimary))
                .setVibrate(null)
                .addAction(R.color.transparent, "lock", piLock)
                .addAction(R.color.transparent, "close", piClose);

        Notification n  = mBuilder.build();
        n.flags        |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT; //make it sticky!
        ((NotificationManager) context.getSystemService(NOTIFICATION_SERVICE)).notify(333, n);
    }




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
